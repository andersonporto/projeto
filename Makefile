NAME	= test

CC		=	gcc
CFLAGS	=	-Wall -Wextra
INCLUDE =	-I ./include

LIBS	= -lcurl -ljson-c -lmicrohttpd -lssl -lcrypto -DMG_ENABLE_OPENSSL -pthread
LIBFT	= -L ./source/libs/libft/ -lft ./source/libs/zlog/src/libzlog.a

S_FOLDER = ./source/
L_FOLDER = libs/

HEADER	= ./include/headers.h

LIB =			$(addprefix $(L_FOLDER), \
		mJson.c mongoose.c \
)

SRC =			$(addprefix $(S_FOLDER), \
		$(LIB) \
		main.c routes.c server.c \
)

.c.o:
	$(CC) $(CFLAGS) $(INCLUDE) $(LIBS) -c $< -o $(<:%.c=%.o)

OBJ = $(SRC:%.c=%.o)

all: $(NAME)

$(NAME): $(OBJ) $(HEADER)
	rm -rf $(NAME)
	make all -C ./source/libs/libft
	$(CC) $(CFLAGS) $(OBJ) $(LIBFT) $(LIBS) -o $(NAME)

clean:
	make clean -C ./source/libs/libft
	rm -rf $(OBJ)
	rm -rf ./a.out

fclean: clean
	make fclean -C ./source/libs/libft
	rm -rf $(NAME)

re: fclean all

.PHONY: all bonus clean fclean re push

push:fclean
	git add .
	read -p "Message:" message; \
	git commit -m "$$message"; \
	git push
